# !/bin/bash

#------------------------------------------------------------------------------------------------------
#
# University of Colorado, Boulder
# APES Homework 1
# Author : Kiran Hegde
# Date   : 1/23/2018
# Description : This file downloads the 4.14.14 version kernel and installs it
# Note : System will restart at the end
#
#------------------------------------------------------------------------------------------------------ 
clear

set -e

echo "apt-get updating"
echo
sudo apt-get update
echo
echo "Apt-get update complete"
echo
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "Installing Necessary Dependencies"
sudo apt-get install libncurses-dev
sudo apt-get install libelf-dev
sudo apt-get install libssl-dev

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "Downloading the kernel from kernel source"
wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.14.14.tar.xz
echo "Downloading Completed"
echo
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "Untaring the downloaded file"
tar -xf linux-4.14.14.tar.xz
echo "Untaring is done"
echo
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
cd linux-4.14.14
make defconfig
echo "Compiling the kernel"
make -j 4
make modules
echo "Installing the Kernel"
sudo make modules_install
sudo make install
echo "Installation is Complete"
echo
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo
echo "System will reboot now"
sudo reboot

